package com.hz.controller;

import com.hz.utils.RedisUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class QgController {

    @Resource
    private RedisUtil redisUtil;

    @RequestMapping("/qg")
    public  String qg(String userId,String goodsId){

        String lockkey = "lock_goods_"+goodsId;
        while(!redisUtil.lock(lockkey,60L)){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

       String res = "";
       String flag =  redisUtil.getStr(userId+":"+goodsId);
       if(flag!=null&&"1".equals(flag))
       {
           redisUtil.unLock(lockkey);//解锁
            return "用户"+userId+"已经抢购过此商品！！！！";
       }

        String stock = redisUtil.getStr("goods_"+goodsId);
        int stockgoods = Integer.parseInt(stock);
        if(stockgoods>=1)
        {
            stockgoods = stockgoods - 1;
            redisUtil.setStr( "goods_"+goodsId,stockgoods+"");
            //添加用户购买记录
            redisUtil.setStr("qg:"+userId+":"+goodsId,"1");



            res = "用户"+userId+"抢到一个商品!!!";

        }else{

            res = "库存不足!用户"+userId+"没有抢到";
        }
        redisUtil.unLock(lockkey);//解锁
        return  res;

    }




}

package com.hz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QgDemo1Application {

    public static void main(String[] args) {
        SpringApplication.run(QgDemo1Application.class, args);
    }

}
